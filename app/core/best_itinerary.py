from core.google_maps import GoogleMapsGeocodeAPI
from core.itinerary import AtomicItinerary, VelibItinerary, AutolibItinerary
from core.exceptions import APIConnectionError, GetItineraryError
from core.openweather import OpenWeatherAPI


def best(address_1, address_2, option_choisie, charge):
    """Function that takes for input :
    - a starting address
    - an ending address
    - the critera chosen between 'rapide', 'court' or 'rapide_pied'
    - a bolean : True if the passenger is loaded, False if not

    and returning the tuple (isValid, best_iti, informations) where :
    - isValid, bolean : True if at least one itinerary was able to be calculated, False if neither itineray could be found
    - best_iti : the best itinerary, as an object of the class Itinerary
    - informations, a dictionnary : KEYS=travelling mode, VALUES=pretty_duration of the itineray corresponding or an error message if not possible to calculate the corresponding itinerary"""

    # Transforming of the addresses into coords
    coords_1 = GoogleMapsGeocodeAPI().geocode(address_1)
    coords_2 = GoogleMapsGeocodeAPI().geocode(address_2)
    isValid = False
    informations = {
        'transit': '',
        'velib': '',
        'autolib': ''
    }

    # Calculating of the three itineraries, if possible - Else, raising an error
    # Transit Itinerary
    try:
        transit = AtomicItinerary(coords_1, coords_2, 'transit')
        transit.getItinerary()
        isValid = True
        informations['transit'] = transit.pretty_duration
    except TypeError:
        print('typeError')
        informations['transit'] = 'Aucun itinéraire n\'a été trouvé.'
    except APIConnectionError as err:
        print('APIConnection')
        informations['transit'] = err.message
    except GetItineraryError as err:
        print('GetItiError')
        informations['transit'] = err.message

    # Velib Itinerary
    try:
        velib = VelibItinerary(coords_1, coords_2)
        velib.getItinerary()
        isValid = True
        informations['velib'] = velib.pretty_duration
    except TypeError:
        print('typeError')
        informations['velib'] = 'Aucun itinéraire n\'a été trouvé.'
    except APIConnectionError as err:
        print('APIConnection')
        informations['velib'] = err.message
    except GetItineraryError as err:
        print('GetItiError')
        informations['velib'] = err.message

    # Autolib Itinerary
    try:
        autolib = AutolibItinerary(coords_1, coords_2)
        autolib.getItinerary()
        isValid = True
        informations['autolib'] = autolib.pretty_duration
    except TypeError:
        print('typeError')
        informations['autolib'] = 'Aucun itinéraire n\'a été trouvé.'
    except APIConnectionError as err:
        print('APIConnection')
        informations['autolib'] = err.message
    except GetItineraryError as err:
        print('GetItiError')
        informations['autolib'] = err.message

    # Taking into account the weather and the load.
    # If the passanger is loaded, we eliminate the Velib Itinerary.
    # If the weather does not allow it, we eliminate the Velib Itinerary.
    try:
        weather = OpenWeatherAPI().getWeather()
        
        if weather in ("Rain", "Snow", "Thunderstorm") or charge == True:
            l = [transit, autolib]
            valid_itineraries = [item for item in l if item.duration != None]
            if charge == True:
                informations['velib'] = "Le Velib n'est pas recommandé quand on est chargé."
            elif weather == "Rain":
                informations['velib'] = "Le Velib n'est pas recommandé quand il pleut."
            elif weather == "Snow":
                informations['velib'] = "Le Velib n'est pas recommandé quand il neige."
            elif weather == "Thunderstorm":
                informations['velib'] = "Le Velib n'est pas recommandé quand c'est la tempête."

        else:
            l = [transit, velib, autolib]
            valid_itineraries = [item for item in l if item.duration != None]

    except APIConnectionError:
        # As the weather is not essential to calculate the itineray, we pass and go on.
        pass

    # Let's take into account the criteria chosen by the passenger.
    try:
        if option_choisie == "rapide":
            min_duration = valid_itineraries[0].duration
            best_iti = valid_itineraries[0]
            for iti in valid_itineraries:
                if iti.duration < min_duration:
                    min_duration = iti.duration
                    best_iti = iti
        elif option_choisie == "court":
            min_distance = valid_itineraries[0].distance
            best_iti = valid_itineraries[0]
            for iti in valid_itineraries:
                if iti.distance < min_distance:
                    min_distance = iti.distance
                    best_iti = iti
        elif option_choisie == "rapide_pied":
            min_foot_duration = valid_itineraries[0].foot_duration
            best_iti = valid_itineraries[0]
            for iti in valid_itineraries:
                if iti.foot_duration < min_foot_duration:
                    min_foot_duration = iti.foot_duration
                    best_iti = iti

    # This error can only occur if neither itinerary was possibly calculated.
    except IndexError:
        isValid = False
        return (isValid, None, informations)

    return (isValid, best_iti, informations)
