# Projet Cachalot

## Installation

- Cloner le répertoire et executer la commande `pip install -r requirements.txt`
- Changer de répertoire jusqu'à 'app' `cd app`
- Executer le programme avec `python manage.py`

## Workflow

### Vérification des adresses
Les adresses sont vérifiées une permière fois automatiquement par le front-end. Si les champs sont vides, les erreurs sont affichées sans appeler l'API. Lorsque les adresses semblent bonnes, le front apelle le back sur la route /validate pour géocoder les adresses et obtenir les coordonnées GPS pour pouvoir calculer les itinéraires. Si cette opération échoue, les erreurs correspondantes sont affichées sur la page et les champs sont mis en rouge. Si les adresses sont correctes, le front est notifié et rapelle le back une deuxième fois pour obtenir les itinéraires.

### Calcul des itinéraires
Tous les itinéraires sont calculés avec les deux adresses fournies. Le plus proche des consignes est identifié et l'information est renvoyée. On affiche ensuite la réponse, et on donne la possibilité de voir les durées de chaque itinéraire. En cas d'erreur, les différents points où un problème est survenu sont passés au front pour les afficher.

### Logique et partis pris
- Etre chargé est une condition suffisante pour refuser un itinéraire en vélo
- La pluie, la neige et la tempête sont des conditions suffisantes pour refuser un trajet en vélo
- Si un itinéraire ne parvient pas à être calculé pour des raisons autres qu'une erreur de connexion ou de typographie, il est simplement mis de côté
- Une erreur relative à la météo n'est pas suffisante pour impacter l'itinéraire, on fera juste l'hypothèse que le soleil brille