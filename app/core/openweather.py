import requests
import json
from core.exceptions import APIConnectionError

class OpenWeatherAPI():
    __api_key = "8ff9d14ca306ecc266f87f2757c68390"
    __url = "https://api.openweathermap.org/data/2.5/weather?q=Paris"
   
    def getWeather(self):
        """Recalculating each time Paris' weather."""
        parameters = {
            "appid": OpenWeatherAPI.__api_key 
        }

        try:
            r = requests.get(OpenWeatherAPI.__url, parameters).content.decode()
        except requests.exceptions.ConnectionError:
            raise APIConnectionError
        
        weather = json.loads(r)["weather"][0]["main"]
        return(weather)