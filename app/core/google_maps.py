import requests
import json
from core.exceptions import APIConnectionError, AddressError, GetItineraryError

class GoogleMapsGeocodeAPI():
    __api_key = "AIzaSyCSQpWcAxE33aYiFBHxIyVgZbJex_XJwds"
    __url = "https://maps.googleapis.com/maps/api/geocode/json"

    def geocode(self, address):
        """From a free-text adress, get the coords using GMaps output in a tuple (lat, lng)"""
        # Parameters for the API
        parameters = {
            "address": address,
            "key": GoogleMapsGeocodeAPI.__api_key
        }

        # GET request to the API and testing if the adress is correct.
        try:
            r = requests.get(GoogleMapsGeocodeAPI.__url, parameters)
        except requests.exceptions.ConnectionError:
            raise APIConnectionError

        # Fetch the results
        r = r.content.decode()

        # Parse the JSON
        j = json.loads(r)
        try:
            lat = j["results"][0]["geometry"]["location"]["lat"]
            lng = j["results"][0]["geometry"]["location"]["lng"]
        except AttributeError:
            raise AddressError(address)
        except IndexError:
            raise AddressError(address)
        else:
            # Returning a tuple
            return (lat, lng)


class GoogleMapsDirectionsAPI():
    __api_key = 'AIzaSyCyPUTsvzcTXKQLsKChARPfbfuDlOMerd4'
    __url = "https://maps.googleapis.com/maps/api/directions/json"

    def getItinerary(self, depart, arrivee, mode):
        """Get itinerary by foot from two coords tuples (lat, lng), (lat, lng) output is a itinerary object"""

        # Parameters for the API
        parameters = {
            "origin": "{}, {}".format(depart[0], depart[1]),
            "destination": "{}, {}".format(arrivee[0], arrivee[1]),
            "key": GoogleMapsDirectionsAPI.__api_key,
            "mode": mode,
            "units": "metric"
            }

        # GET request to the API
        try:
            r = requests.get(GoogleMapsDirectionsAPI.__url, parameters)
        except requests.exceptions.ConnectionError:
            raise APIConnectionError
        
        # Fetch the results
        r = r.content.decode()

        # Parse the JSON
        j = json.loads(r)
        try:
            duration = j["routes"][0]["legs"][0]["duration"]["value"]
            distance = j["routes"][0]["legs"][0]["distance"]["value"]
        except IndexError:
            raise GetItineraryError('No itinerary was found')
        else:
            # Returning a dictionnary
            return { "duration": duration, "distance": distance }