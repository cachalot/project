from flask import Flask, jsonify, render_template, request
import json
from core import google_maps, exceptions, best_itinerary

# Create the app
app = Flask(__name__)

# Main route to render the page
@app.route('/', methods=['GET'])
def hello():
    if request.method == 'GET':
        return render_template('app.html')

# Validation route
@app.route('/api/validate', methods=['POST'])
def validate_data():
    # Validate the form data
    content = request.data.decode()
    # Load it into a JSON
    j = json.loads(content)
    # Print info for debugging
    print('Received data from client : {}'.format(j))
    res = {'address_1': False, 'address_2': False, 'errors':[]}

    try:
        google_maps.GoogleMapsGeocodeAPI().geocode(j['start'])
        res['address_1'] = True
    except exceptions.APIConnectionError:
        res['errors'].append("Veuillez vérifier votre connexion internet.")
    except exceptions.AddressError:
        res['errors'].append("L'adresse de départ semble incorrecte.")

    try:
        google_maps.GoogleMapsGeocodeAPI().geocode(j['end'])
        res['address_2'] = True
    except exceptions.APIConnectionError:
        res['errors'].append("Veuillez vérifier votre connexion internet.")
    except exceptions.AddressError:
        res['errors'].append("L'adresse d'arrivée semble incorrecte.")

    # Return the data to the client
    return json.dumps(res), 200, {'ContentType':'application/json'}

# Itinerary route
@app.route('/api/itinerary', methods=['POST'])
def get_itinerary():
    # Validate the form data
    content = request.data.decode()
    # Load it into a JSON
    j = json.loads(content)
    # Print info for debugging
    print('Received data from client : {}'.format(j))

    options = [('rapide',j['rapide']), ('court',j['court']), ('rapide_pied',j['rapide_pied'])]
    option_choisie = [x for x in options if x[1] == True][0][0]
    best_iti_res = best_itinerary.best(j['start'], j['end'], option_choisie, j['charge'])

    # bes_iti_res scheme: (isValid, best_itinerary, informations)
    if best_iti_res[0] == True:
        # Itinerary exists
        # Testing the best itinerary type
        best_iti = best_iti_res[1]
        if str(type(best_iti)) == "<class 'core.itinerary.AtomicItinerary'>":
            best_iti_type = 'Transports en commun'
        if str(type(best_iti)) == "<class 'core.itinerary.AutolibItinerary'>":
            best_iti_type = 'Autolib'
        if str(type(best_iti)) == "<class 'core.itinerary.VelibItinerary'>":
            best_iti_type = 'Velib'

        res = {
            'isValid': True,
            'duration': best_iti.pretty_duration,
            'type': best_iti_type,
            'informations': best_iti_res[2]}
        return json.dumps(res), 200, {'ContentType': 'application/json'}
    
    else:
        # No itinerary exists
        res = {
            'isValid': False,
            'duration': None,
            'type': None,
            'informations': best_iti_res[2]}
        return json.dumps(res), 200, {'ContentType': 'application/json'}


if __name__ == '__main__':
    app.run()