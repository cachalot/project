import requests
import json
from core.exceptions import GetItineraryError, APIConnectionError


class OpenParisAPI():
    __base_url = 'https://opendata.paris.fr/api/records/1.0/search?'
    __lang = 'fr'
    __rows = '-1'
    __datasets = {
        'Velib': 'stations-velib-disponibilites-en-temps-reel',
        'Autolib': 'autolib-disponibilite-temps-reel'
    }

    def getClosestVelibStation(self, coords):
        """From a tuple of coords, get the coords of the closest Velib station. If not found, raise an error."""

        lat, lng = coords
        geofilter = str(lat) + ',' + str(lng) + ',' + str(1000)
        parameters = {
            'dataset': self.__datasets['Velib'],
            'lang': self.__lang,
            'rows': self.__rows,
            'geofilter.distance': geofilter
        }

        # GET request to API
        try:
            r = requests.get(self.__base_url, parameters)
        except requests.exceptions.ConnectionError:
            raise APIConnectionError

        # Fetch the results
        r = r.content.decode()

        # Parse the JSON
        j = json.loads(r)
        if len(j['records']) == 0:
            raise GetItineraryError("Il n'est pas possible d'effectuer ce trajet en Vélib.")
        else:
            station_coords = j['records'][0]['geometry']['coordinates']
            station_lng = station_coords[0]
            station_lat = station_coords[1]

        return (station_lat, station_lng)



    def getClosestAutolibStation(self, coords):
        """From a tuple of coords, get the coords of the closest Autolib station. If not found, raise an error."""
        
        # Add an exception if the coords are not good
        lat, lng = coords
        geofilter = str(lat) + ',' + str(lng) + ',' + str(1000)
        parameters = {
            'dataset': self.__datasets['Autolib'],
            'lang': self.__lang,
            'rows': self.__rows,
            'geofilter.distance': geofilter
        }

        # GET request to API
        try:
            r = requests.get(self.__base_url, parameters)
        except requests.exceptions.ConnectionError:
            raise APIConnectionError

        # Fetch the results
        r = r.content.decode()

        # Parse the JSON
        j = json.loads(r)
        if len(j['records']) == 0:
            raise GetItineraryError("Il n'est pas possible d'effectuer ce trajet en Autolib.")
        else:
            station_coords = j['records'][0]['geometry']['coordinates']
            station_lng = station_coords[0]
            station_lat = station_coords[1]

        return (station_lat, station_lng)