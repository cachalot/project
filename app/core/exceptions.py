class Error(Exception):
    """Base class for our exceptions"""
    pass

class APIConnectionError(Error):
    """The connexion with the API failed"""
    def __init__(self):
        self._message = "Vérifiez votre connection internet."
    
    @property
    def message(self):
        return self._message

class AddressError(Error):
    """Error if address entered invalid when trying to get geo coord"""
    def __init__(self, bad_address):
        self._bad_address = bad_address
    
    @property
    def bad_address(self):
        return self._bad_address

class GetItineraryError(Error):
    """Error with the result from the API"""
    def __init__(self, message):
        self._message = message
    
    @property
    def message(self):
        return self._message