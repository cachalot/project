/* Wait for the page to be loaded */
$(document).ready(function() {
    // Form data for the validation (Semantic)
    // Client-side validation
    $('.ui.form')
    .form({
        on: 'blur',
        fields: {
            address_1: {
                identifier: 'address_1',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Veuillez entrer une adresse de départ'
                    }
                ]
            },
            address_2: {
                identifier: 'address_2',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Veuillez entrer une adresse d\'arrivée'
                    }
                ]
            }
        }
    });

    // Trigger for the informations table
    $('#info_label').click(function() {
        $('#informations').css("display", "block")
    })

    // Validation function when the form is submitted
    $('#submit').click(function(e) {
        // Prevent the default action of the form
        e.preventDefault();
        // Empty the fields if they were filled by a previous request
        $('#error_message').css('display', 'none')
        $('#result_duration').text('');
        // Show the loader
        $('#success_message').css('display', 'block')
        $('#result_type').html('<div class="ui active inverted dimmer"><div class="ui tiny text active loader">Loading</div></div>');

        // Client-side validation
        $('#form').form('validate form');
        // Is the form valid ?
        if($('#form').form('is valid')){
            // Calling the API
            $.ajax({
                type: "POST",
                url: '/api/validate',
                contentType: 'application/json; charset=UTF-8',
                data: JSON.stringify({
                    start: $(".address#1").val(),
                    end: $(".address#2").val(),
                }),
                success: onSuccess,
                error: onError
            })
        }
    })

    // If the server responded with a 2xx response
    function onSuccess(data) {
        // Load the data into a js object
        d = JSON.parse(data)
        // Handle the errors if needed
        if(!d.address_1 || !d.address_2){
            $('#form').form('add errors', d.errors)
        } else {
            // Get the itinerary
            $.ajax({
                type: 'POST',
                url: 'api/itinerary',
                contentType: 'application/json; charset=UTF-8',
                data: JSON.stringify({
                    start: $(".address" + '#' + String(1)).val(),
                    end: $(".address" + '#' + String(2)).val(),
                    rapide: $('#rapide').is(':checked'),
                    court: $('#court').is(':checked'),
                    rapide_pied: $('#rapide_pied').is(':checked'),
                    charge: $('#charge').is(':checked')
                }),
                success: onSuccessIti,
                error: onErrorIti
            })
        }
    }

    // If the server responded with an error
    function onError(data) {
        console.log('Erreur lors de la connexion au serveur')
    }

    // If the server responded with a 2xx type response for the itinerary
    function onSuccessIti(data) {
        // Load the data
        d = JSON.parse(data)

        // Load the informations into the table
        $('#transit_info').text(d.informations.transit)
        $('#velib_info').text(d.informations.velib)
        $('#autolib_info').text(d.informations.autolib)

        // If at least one valid itinerary was found
        if(d.isValid) {
            // Display the best itinerary
            $('#result_type').text("L'itinéraire recommandé est : " + d.type)
            $('#result_duration').text("Durée du trajet : " + d.duration)

            // Put the ribbon on the best itinerary
            $('#transit_flag').removeClass('ui ribbon label');
            $('#velib_flag').removeClass('ui ribbon label');
            $('#autolib_flag').removeClass('ui ribbon label');
            switch(d.type){
                case 'Transports en commun':
                    $('#transit_flag').addClass('ui ribbon label');
                    break;
                case 'Velib':
                    $('#velib_flag').addClass('ui ribbon label');
                    break;
                case 'Autolib':
                    $('#autolib_flag').addClass('ui ribbon label');
                    break;
            }
        } else {
            // No valid itinerary was found
            $('#success_message').css("display", "none")
            $('#error_message').css("display", "block").text("Aucun itinéaire n'a été trouvé.")
        }
    }

    // The server responded with an error
    function onErrorIti(data) {
        $('#error_message').text('Veuillez-nous excuser, une erreur inconnue a surgi.')
    }
});
// End script