from core.google_maps import GoogleMapsDirectionsAPI
from core.open_paris import OpenParisAPI
from core.exceptions import GetItineraryError, APIConnectionError, AddressError

class Itinerary():
    """Main Class for itineraries"""
    def __init__(self, start, end):
        self._error = False
        self._start = start
        self._end = end
        self._duration = None
        self._pretty_duration = None
        self._distance = None
        self._mode = None
        self._foot_duration = None
        self._pretty_foot_duration = None
    
    def __repr__(self):
        return "Le trajet va durer {} secondes et {}m.".format(self._duration, self._distance)

    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, value):
        if type(value) != tuple:
            raise TypeError("The class Itinerary expects a tuple for the origin address.")
    
    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, value):
        if type(value) != tuple:
            raise TypeError("The class Itinerary expects a tuple for the destination address.")

    @property
    def duration(self):
        if self._duration != None:
            return self._duration
        else:
            return None

    @property
    def pretty_duration(self):
        if self._duration != None:
            m, s = divmod(self._duration, 60)
            h, m = divmod(m, 60)
            if h != 0:
                return str(h) + " h " + str(m) + " min " + str(s) + " sec"
            else:
                return str(m) + " min " + str(s) + " sec"
        else:
            return None

    @property
    def distance(self):
        return self._distance

    @property
    def mode(self):
        return self._mode

    @property
    def foot_duration(self):
        return self._foot_duration

    @property
    def pretty_foot_duration(self):
        if self._foot_duration != None:
            m, s = divmod(self._foot_duration, 60)
            h, m = divmod(m, 60)
            if h != 0:
                return str(h) + " h " + str(m) + " min " + str(s) + " sec"
            else:
                return str(m) + " min " + str(s) + " sec"
        else:
            return None

class AtomicItinerary(Itinerary):
    """Generic small Itinerary"""
    def __init__(self, start, end, mode):
        Itinerary.__init__(self, start, end)
        self._mode = mode

    def getItinerary(self):
        # There can be no error here
        d = GoogleMapsDirectionsAPI().getItinerary(self._start, self._end, self._mode)

        self._duration = int(d["duration"])
        if self._mode == 'foot':
            self._foot_duration = d["duration"]
        else:
            self._foot_duration = 0
        self._distance = d["distance"]

class AutolibItinerary(Itinerary):
    """Generic Autolib Itinerary"""
    def __init__(self, start, end):
        Itinerary.__init__(self, start, end)

    def getItinerary(self):
        """Computes the global itinerary"""
        try:
            # Finding the nearest Autolib stations
            station_start = OpenParisAPI().getClosestAutolibStation(self._start)
            station_end = OpenParisAPI().getClosestAutolibStation(self._end)

            # Finding the different parts of the global itinerary
            iti_pied_1 = AtomicItinerary(self._start, station_start, 'walking')
            iti_pied_2 = AtomicItinerary(station_end, self._end, 'walking')
            iti_voiture = AtomicItinerary(station_start, station_end, 'driving')

            # Calculating the itineraries' attributes
            iti_pied_1.getItinerary()
            iti_pied_2.getItinerary()
            iti_voiture.getItinerary()

        except GetItineraryError as err:
            self._error = True
            raise err

        except APIConnectionError as err:
            self._error = True
            raise err
        
        else:
            # Summing the attributes to get the global itinerary's attributes
            self._duration = iti_pied_1.duration + iti_pied_2.duration + iti_voiture.duration
            self._foot_duration = iti_pied_1._foot_duration + iti_pied_2._foot_duration + iti_voiture._foot_duration
            self._distance = iti_pied_1.distance + iti_pied_2.distance + iti_voiture.distance

class VelibItinerary(Itinerary):
    """Generic Velib Itinerary"""
    def __init__(self, start, end):
        Itinerary.__init__(self, start, end)

    def getItinerary(self):
        """Computes the global itinerary"""
        # Finding the nearest Velib stations
        try:
            station_start = OpenParisAPI().getClosestVelibStation(self._start)
            station_end = OpenParisAPI().getClosestVelibStation(self._end)

            # Finding the different parts of the global itinerary
            iti_pied_1 = AtomicItinerary(self._start, station_start, 'walking')
            iti_pied_2 = AtomicItinerary(station_end, self._end, 'walking')
            iti_velo = AtomicItinerary(station_start, station_end, 'bicycling')

            # Calculating the itineraries' attributes
            iti_pied_1.getItinerary()
            iti_pied_2.getItinerary()
            iti_velo.getItinerary()

        except GetItineraryError as err:
            self._error = True
            raise err
        
        except APIConnectionError as err:
            self._error = True
            raise err

        else:
            # Summing the attributes to get the global itinerary's attributes
            self._duration = iti_pied_1.duration + iti_pied_2.duration + iti_velo.duration
            self._foot_duration = iti_pied_1.foot_duration + iti_pied_2.foot_duration + iti_velo.foot_duration
            self._distance = iti_pied_1.distance + iti_pied_2.distance + iti_velo.distance
